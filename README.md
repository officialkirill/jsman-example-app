# Näidisrakenduse failid
* /app põhilised süsteemifailid
* **/app/src/jsman.php** JSMani kood
* /cache/ smarty vahemälu jaoks vajalik kaust
* /public avaliku poole juurkaust, Apache serveri puhul on tavaliselt selle kausta nimeks /htdocs.
Kuna näidisrakendus tõenäoliselt avalikus juurkaustas ei asu, pole vaja /public kaustas olevaid faile kusagile mujale tõsta

# Paigaldamine

1. Lae failid enda serverisse üles
2. Ava app/config.php fail, muuda seal SITE_URL konstandi väärtus selliseks, mis vastaks sinu serveri aadressile ja rakenduse asukohale
3. Anna kaustale /cache/smarty/compile chmod õigused 777
4. Anna kaustale /public/generated_javascript samuti chmod 777 õigused
5. Suuna browser /public/index.php lehele

# Veaotsing
Kui midagi on valesti siis alusta sellest, et mine /public/check.php lehele

# Töötav näide
Rakendus ise: http://kirill.rada7.ee/jsman-example-app/public
check.php: http://kirill.rada7.ee/jsman-example-app/public/check.php