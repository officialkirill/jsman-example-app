<?php

# only change this setting
define("SITE_URL", "http://localhost/jsman-example-app/public/");

# change this to the app's root path, if you're on a *NIX system and are having problems with incorrect paths
define("ROOT_PATH", realpath(dirname(__FILE__) . "../../") . DIRECTORY_SEPARATOR);

# dont touch these unless you know what you're doing
# ----------------------------------------------------------------------------------------------------------------------
$generated_js_folder_name = 'generated_javascript';

define("APP_PATH", ROOT_PATH . 'app' . DIRECTORY_SEPARATOR);
define("PUBLIC_PATH", ROOT_PATH . "public" . DIRECTORY_SEPARATOR);
define("VENDORS_PATH", APP_PATH . "vendor" . DIRECTORY_SEPARATOR);
define("CACHE_PATH", ROOT_PATH . "cache" . DIRECTORY_SEPARATOR);
define("SRC_PATH", APP_PATH . "src" . DIRECTORY_SEPARATOR);

define("JAVASCRIPT_PATH", SRC_PATH . "js/");
define("GENERATED_JS_PATH", PUBLIC_PATH . DIRECTORY_SEPARATOR . $generated_js_folder_name . "/");
define("JS_URL", SITE_URL . $generated_js_folder_name);