<?php

class gzipman
{
    public function __construct()
    {
        if (function_exists('gzopen'))
            $this->gzip_enabled = true;
        else
            $this->gzip_enabled = false;
    }

    public function compress($src_file, $data)
    {
        if ($this->gzip_enabled !== true)
            return;

        // open file for writing with maximum compression
        $open_file = gzopen($src_file, 'w9');

        // write string to file
        gzwrite($open_file, $data);

        // close file
        gzclose($open_file);
    }

    public function read($file)
    {
        if ($this->gzip_enabled !== true)
            return;

        $open_file = gzopen($file, 'r');
        gzpassthru($open_file);
        gzclose($open_file);
    }
}
