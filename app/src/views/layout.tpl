<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Demo</title>

    <!-- css -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap-theme.min.css"
          rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 70px;
            padding-bottom: 30px;
        }
    </style>

    <!-- JSMan -->
    {if isset($javascript_url)}
        <script src="{$javascript_url}" type="text/javascript"></script>
    {/if}
</head>
<body>

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="{$links.home}">Esileht</a></li>
                <li><a href="{$links.demo}">Demo</a></li>
                <li><a href="{$links.clear}">Clear cache</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>
<div class="container">
    <p>Siin lehel kasutuses olev javascript asub aadressil: <a href="{$javascript_url}" target="_blank">{$javascript_url}</a></p>
    {if isset($content)}
        {$content}
    {/if}
</div>

</body>
</html>
