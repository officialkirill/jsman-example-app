<?php

require_once VENDORS_PATH . 'JavaScriptPacker.php';
require_once SRC_PATH . 'gzipman.php';

class jsman
{
    /**
     * @param string $js_path
     * @param string $js_cache_path
     * @param string $js_url
     * @param bool $use_packer
     * @param array $default_scripts
     */
    public function __construct($js_path, $js_cache_path, $js_url, $use_packer = true, array $default_scripts = array())
    {
        // the path we take javascript from
        $this->js_path = $js_path;

        // the path where we save generated cache file
        $this->js_cache_path = $js_cache_path;

        // the url location to js files
        $this->js_url_path = $js_url;

        // generated filename
        $this->hash_name = null;

        // hash algorithm we use for naming the generated file
        $this->hash_algorithm = 'sha1';

        // cache status
        // true - cache is set to work
        // false - cache disabled, js file is generated on each page load
        $this->cache_enabled = true;

        // we'll be collecting javascript files into this array
        $this->loaded_scripts = array();

        // default scripts
        $this->default_scripts = $default_scripts;

        // also, let's collect some js variables
        $this->variables = array();

        // use packer?
        $this->use_packer = $use_packer;

        // gzip packer
        $this->gzip = new gzipman();

        // load default scripts
        $this->load_defaults();
    }

    /**
     * @return array
     */
    public function get_default_scripts()
    {
        return (array)$this->default_scripts;
    }

    /**
     * @return null
     */
    private function get_hash_name()
    {
        return $this->hash_name;
    }

    /**
     * @param null $hash_name
     */
    private function set_hash_name($hash_name)
    {
        $this->hash_name = $hash_name;
    }

    /**
     * build the file
     * @return    void
     */
    public function init()
    {
        $hash_name = $this->create_cache_name();

        // load or create cache file
        if (
            $this->cache_file_exists($hash_name) === false
            AND !empty($this->loaded_scripts)
        )
            $this->create_cache_file($hash_name, $this->loaded_scripts);
    }

    /**
     * add additional scripts
     * @param mixed $scripts
     * @param bool $clear_previous clears previously added scripts
     */
    public function add($scripts = null, $clear_previous = false)
    {
        // clear previously saved scripts
        if ($clear_previous === true)
            $this->empty_stockpile();

        // no scripts given
        if (empty($scripts))
            return;

        // check if the input scripts list is an array or a string
        // if it's a string, turn it into an array
        // note! this is used for one script additions
        if (!is_array($scripts))
            $scripts = array($scripts);

        // flatten multidimensional arrays
        $scripts = $this->flatten_array($scripts);

        $this->loaded_scripts = array_unique(array_merge($this->loaded_scripts, $scripts));
    }

    /**
     * load default scripts
     * @param bool $clear_previous
     */
    public function load_defaults($clear_previous = false)
    {
        if (!is_bool($clear_previous))
            $clear_previous = false;

        $scripts = $this->get_default_scripts();

        $this->add($scripts, $clear_previous);
    }

    /**
     * return an array of currently stored in scripts
     * @return    array
     */
    public function stockpile()
    {
        return $this->loaded_scripts;
    }

    /**
     * empties currently collected js files
     * @return    void
     */
    public function empty_stockpile()
    {
        $this->loaded_scripts = array();
    }

    /**
     * creates a name for the js cache file
     * @param array $scripts
     * @return string
     */
    private function create_cache_name(array $scripts = array())
    {
        if (empty($scripts))
            $scripts = $this->loaded_scripts;

        $this->set_hash_name(hash($this->hash_algorithm, implode('', $scripts)));
        return $this->get_hash_name();
    }

    /**
     * creates a merged file both in js and gzip formats
     * @param $hash_name
     * @param array $scripts
     */
    private function create_cache_file($hash_name, array $scripts = array())
    {
        if (empty($scripts))
            $scripts = $this->loaded_scripts;

        // merge content
        $merged_content = $this->merge_files($scripts); // this is a string

        // create js file
        $cache_file = fopen($this->js_cache_path . $hash_name . '.js', 'w');
        fwrite($cache_file, $merged_content);
        fclose($cache_file);

        // gzip file
        $this->gzip->compress($this->js_cache_path . $hash_name . '.js.gz', $merged_content);
    }

    /**
     * merge javascript files into one string and return the string created
     * @param        array
     * @return    string
     */
    private function merge_files($files = array())
    {
        // continue with merge
        $merged_file_contents = '';

        // add some default js variables
        $merged_file_contents .= $this->js_variables();

        if (empty($files) OR !is_array($files))
            $files = $this->loaded_scripts;

        if (!empty($files)) {
            foreach ($files as $file) {
                // stop if no file found
                if (!$this->js_file_exists($file))
                    continue;

                $file_contents = file_get_contents($this->js_path . $file);

                // pack unpacked files, check if .min or .pack is in filename, if it is, skip the file
                if (
                    !preg_match('/\.min\./i', $file)
                    AND !preg_match('/\.pack\./i', $file)
                ) {
                    $packed = $this->pack($file_contents);
                    $merged_file_contents .= $packed;
                } else
                    $merged_file_contents .= $file_contents;

            }
        }

        // return the results
        return $merged_file_contents;
    }

    /**
     * add some js variables with php, so we won't have to manually edit js files
     * @return    string
     */
    private function js_variables()
    {
        $variables = array(
            'php_version' => phpversion(),
        );
        $all_variables = array_merge($variables, $this->variables);
        $return = '';

        foreach ($all_variables as $key => $var) {
            $return .= 'var ' . $key . ' = "' . $var . '";';
            $return .= "\n";
        }

        return $this->pack($return);
    }

    /**
     * @deprecated
     * @param $name
     * @param $value
     */
    public function assign_var($name, $value)
    {
        $this->variables = array_merge($this->variables, array($name => $value));
    }

    /**
     * check if cached file exists
     * @param        string
     * @return    bool
     */
    private function cache_file_exists($hash_name = null)
    {
        if ($this->cache_enabled !== true)
            return false;

        $full_path = $this->js_cache_path . $hash_name . '.js';

        if (
            file_exists($full_path)
            AND is_readable($full_path)
            AND is_file($full_path)
        )
            return true;
        else
            return false;

    }

    /**
     * check to see if javascript script exists
     * @param            string
     * @return        bool
     */
    private function js_file_exists($script = null)
    {
        $full_path = $this->js_path . $script;

        if (
            file_exists($full_path)
            AND is_readable($full_path)
            AND is_file($full_path)
        )
            return true;
        else
            return false;
    }

    /**
     * delete all created cache files
     * @return    void
     */
    public function clear_cache()
    {
        $this->delete_files($this->js_cache_path);
    }

    /**
     * @return string
     */
    public function get_cache_file_url()
    {
        return $this->js_url_path . '/' . $this->get_hash_name() . '.js';
    }

    /**
     * pack given string with JS packer/minifier
     * @param        string
     * @return    string
     */
    private function pack($string = null)
    {
        if (empty($string))
            return $string;

        // check if we need to pack
        if ($this->use_packer !== true)
            return $string;

        $packer = new JavaScriptPacker($string, 0, true, false);

        return $packer->pack();
    }

    private function flatten_array(array $array)
    {
        $ret_array = array();
        foreach (new RecursiveIteratorIterator(new RecursiveArrayIterator($array)) as $value) {
            $ret_array[] = $value;
        }
        return $ret_array;
    }

    /**
     * taken from Codeigniter
     * @param $path
     * @param bool $del_dir
     * @param int $level
     * @return bool
     */
    private function delete_files($path, $del_dir = false, $level = 0)
    {
        // Trim the trailing slash
        $path = rtrim($path, DIRECTORY_SEPARATOR);

        if (!$current_dir = @opendir($path)) {
            return false;
        }

        while (false !== ($filename = @readdir($current_dir))) {
            if ($filename !== '.' AND $filename !== '..') {
                if (is_dir($path . DIRECTORY_SEPARATOR . $filename) && $filename[0] !== '.') {
                    $this->delete_files($path . DIRECTORY_SEPARATOR . $filename, $del_dir, $level + 1);
                } else {
                    unlink($path . DIRECTORY_SEPARATOR . $filename);
                }
            }
        }
        @closedir($current_dir);

        if ($del_dir == true AND $level > 0) {
            return @rmdir($path);
        }

        return true;
    }
}
