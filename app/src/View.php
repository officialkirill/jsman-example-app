<?php

require_once VENDORS_PATH . "smarty/Smarty.class.php";
require_once 'jsman.php';

class View extends Smarty
{
    public $jsman;

    public function __construct()
    {
        parent::__construct();

        // smarty config
        $this->setTemplateDir(realpath(SRC_PATH . 'views/'));
        $this->setCompileDir(realpath(CACHE_PATH));
        $this->setConfigDir(realpath(SRC_PATH));
    }

    public function display_content($template)
    {
        $this->assign('content', $this->fetch($template));
        $this->display('layout.tpl');
    }
}