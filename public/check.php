<?php
require_once "../app/config.php";
function is__writable($path)
{
    // http://php.net/manual/en/function.is-writable.php
    //will work in despite of Windows ACLs bug
    //NOTE: use a trailing slash for folders!!!
    //see http://bugs.php.net/bug.php?id=27609
    //see http://bugs.php.net/bug.php?id=30931

    if ($path{strlen($path) - 1} == '/') // recursively return a temporary file path
        return is__writable($path . uniqid(mt_rand()) . '.tmp');
    else if (is_dir($path))
        return is__writable($path . '/' . uniqid(mt_rand()) . '.tmp');
    // check tmp file for read/write capabilities
    $rm = file_exists($path);
    $f = @fopen($path, 'a');
    if ($f === false)
        return false;
    fclose($f);
    if (!$rm)
        unlink($path);
    return true;
}

$results = array();

// php version
if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
    $results['phpversion'] = 'success';
} else {
    $results['phpversion'] = 'danger';
}

// smarty cache
$smarty_compile_path = CACHE_PATH;
$results['smarty_compile'] = is__writable($smarty_compile_path) ? 'success' : 'danger';

// js gen
$results['js_gen'] = is__writable(GENERATED_JS_PATH) ? 'success' : 'danger';
?>
<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>check</title>

    <!-- css -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/css/bootstrap-theme.min.css"
          rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 70px;
            padding-bottom: 30px;
        }

        .label:empty {
            display: inline !important;
        }

        .label-danger:after {
            content: 'Okou!';
        }

        .label-success:after {
            content: 'Korras!';
        }
        .error {
            display: inline;
        }
        .label-success .error {
            display: none;
        }
    </style>
</head>
<body>


<div class="container">
    <ul class="js-results">
        <li>PHP versioon: <?php echo phpversion(); ?>
            <div class="label label-<?php echo $results['phpversion']; ?>">
                <div class="error">Sul on liiga vana php versioon, nõutav on versioon alates 5.3.0</div>
            </div>
        </li>
        <li>Smarty compile kaust
            <div class="label label-<?php echo $results['smarty_compile']; ?>">
                <div class="error">Kontrolli kas kaustale <strong><?php echo $smarty_compile_path; ?></strong> on antud kirjutamise õigused</div>
            </div>
        </li>
        <li>Genereeritud javascripti kaust
            <div class="label label-<?php echo $results['js_gen']; ?>">
                <div class="error">Kontrolli kas kaustale <strong><?php echo GENERATED_JS_PATH; ?></strong> on antud kirjutamise õigused</div>
            </div>
        </li>
    </ul>
    <?php
    if (!in_array('danger', $results)) {
        ?>
        <div class="alert alert-success">korras, testrakendus peaks töötama</div>
    <?php
    } else {
?>
        <div class="alert alert-danger">midagi on valesti, kontrolli kaustade asukohad üle</div>
    <?php
    }
    ?>
</div>

</body>
</html>
