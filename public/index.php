<?php
require_once "../app/config.php";
require_once "../app/src/view.php";

// create instance of view layer
$view = new View();

// create an instance of JSMan
$use_packer = true; # true if you want to use the scripts packing feature
$default_scripts = array('jquery.js'); # default scripts that are included in every generated file
$jsman = new jsman(JAVASCRIPT_PATH, GENERATED_JS_PATH, JS_URL, $use_packer, $default_scripts);

// assign some frontend links
$links = array(
    'home' => SITE_URL,
    'demo' => SITE_URL . '?page=demo',
    'clear' => SITE_URL . '?page=clear_cache'
);
$view->assign('links', $links);

// main switch for different pages
$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
switch ($page) {
    case'clear_cache':
        $jsman->clear_cache();
        $tpl = 'clear.tpl';
        break;
    case'demo':
        $jsman->add('charts/highcharts.js');
        $jsman->add('demo.js');
        $tpl = 'demo.tpl';
        break;
    default:
        $jsman->add('example.js');
        $tpl = 'index.tpl';
}

// jsman init must be located just before we return html
$jsman->init();

// assign
$view->assign('javascript_url', $jsman->get_cache_file_url());

// display generated page with smarty
$view->display_content($tpl);
